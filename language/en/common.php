<?php

/**
 *
 * @package phpBB Extension - View Friends
 * @copyright (c) 2020 Ady
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

/**
 * DO NOT CHANGE
 */
if (!defined('IN_PHPBB'))
{
    exit;
}

if (empty($lang) || !is_array($lang))
{
    $lang = array();
}

// DEVELOPERS PLEASE NOTE
//
// All language files should use UTF-8 as their encoding and the files must not contain a BOM.
//
// Placeholders can now contain order information, e.g. instead of
// 'Page %s of %s' you can (and should) write 'Page %1$s of %2$s', this allows
// translators to re-order the output of data while ensuring it remains correct
//
// You do not need this where single placeholders are used, e.g. 'Message %d' is fine
// equally where a string contains only two placeholders which are used to wrap text
// in a url you again do not need to specify an order e.g., 'Click %sHERE%s' is fine

$lang = array_merge($lang, [
    'VIEWFRIENDS_USERNAME'        => 'Friends of %s',
    'VIEWFRIENDS_NOFRIEND'        => 'You are not friend with this person.',
    'VIEWFRIENDS_REMOVE_TITLE'    => 'Remove a friend',
    'VIEWFRIENDS_CONFIRM_REMOVE'  => 'Do you really remove %s from your friends list?',
    'VIEWFRIENDS_ERROR_ON_REMOVE' => 'Oups, an error occurred ! Please contact an admin.',
]);
