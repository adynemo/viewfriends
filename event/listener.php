<?php

/**
 *
 * @package phpBB Extension - View Friends
 * @copyright (c) 2019 Ady
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\viewfriends\event;

use phpbb\db\driver\driver_interface;
use phpbb\language\language;
use phpbb\path_helper;
use phpbb\template\template;
use phpbb\user;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Event listener
 */
class listener implements EventSubscriberInterface
{
    /**
     * Assign functions defined in this class to event listeners in the core
     *
     * @return array
     * @static
     * @access public
     */
    static public function getSubscribedEvents()
    {
        return [
            "core.memberlist_view_profile" => "render_memberlist_view_profile",
            'core.user_setup'              => 'load_language_on_setup',
        ];
    }

    /** @var driver_interface */
    protected $db;

    /** @var template */
    protected $template;

    /** @var language */
    protected $lang;

    /** @var path_helper */
    protected $path_helper;

    /** @var user */
    protected $user;

    /** @var string table_prefix */
    protected $table_prefix;

    /** @var string table_prefix */
    protected $phpbb_root_path;

    const MAX_SIZE = 30; // Max size img
    /**
     * Constructor
     */
    public function __construct(
        driver_interface $db,
        template         $template,
        language         $lang,
        path_helper      $path_helper,
        user             $user,
        $table_prefix,
        $phpbb_root_path
    ) {
        $this->db              = $db;
        $this->template        = $template;
        $this->lang            = $lang;
        $this->path_helper     = $path_helper;
        $this->user            = $user;
        $this->table_prefix    = $table_prefix;
        $this->phpbb_root_path = $phpbb_root_path;
    }

    /**
     * Adds the template variables for memberlist view profile
     */
    public function render_memberlist_view_profile($event)
    {
        $this->lang->add_lang('common', 'ady/viewfriends');

        $member  = $event['member'];
        $friends = [];

        $sql = 'SELECT z.zebra_id,
            u.username,
            u.user_avatar,
            u.user_avatar_type,
            u.user_avatar_width,
            u.user_avatar_height,
            u.user_colour
            FROM ' . ZEBRA_TABLE . ' z
            INNER JOIN ' . USERS_TABLE . ' u
                ON u.user_id = z.zebra_id
            WHERE z.user_id = ' . (int) $member['user_id'] . '
                AND z.friend = 1
            ORDER BY u.username ASC;';
        $result = $this->db->sql_query($sql);

        while ($row = $this->db->sql_fetchrow($result))
        {
            $row['avatar_img']  = $this->avatar_img_resize($row);
            $row['url_profile'] = get_username_string('full', $row['zebra_id'], $row['username'], $row['user_colour']);
            $friends[(int) $row['zebra_id']] = $row;
        }

        $this->db->sql_freeresult($result);

        $this->template->assign_vars([
            'FRIENDS'              => $friends,
            'VIEWFRIENDS_USERNAME' => $this->lang->lang('VIEWFRIENDS_USERNAME', $member['username']),
            'ALLOWED_TO_REMOVE'    => $this->user->data['user_id'] == $member['user_id'],
        ]);
    }

    public function load_language_on_setup($event)
    {
        $lang_set_ext = $event['lang_set_ext'];
        $lang_set_ext[] = [
            'ext_name' => 'ady/viewfriends',
            'lang_set' => 'common',
        ];
        $event['lang_set_ext'] = $lang_set_ext;
    }

    /**
     * Generate and resize avatar
     *
     * @param array $row
     * @return string
     */
    private function avatar_img_resize($row)
    {
        if (!empty($row['user_avatar']))
        {
            if ($row['user_avatar_width'] >= $row['user_avatar_height'])
            {
                $ratio = $row['user_avatar_width'] / $row['user_avatar_height'];
                $row['user_avatar_height'] = round(self::MAX_SIZE / $ratio);
                $row['user_avatar_width']  = self::MAX_SIZE;
            }
            else
            {
                $ratio = $row['user_avatar_height'] / $row['user_avatar_width'];
                $row['user_avatar_width']  = round(self::MAX_SIZE / $ratio);
                $row['user_avatar_height'] = self::MAX_SIZE;
            }
            return phpbb_get_user_avatar($row);
        }

        // Determine board url - we may need it later
        $board_url      = generate_board_url() . '/';
        $corrected_path = $this->path_helper->get_web_root_path();
        $web_path       = (defined('PHPBB_USE_BOARD_URL_PATH') && PHPBB_USE_BOARD_URL_PATH) ? $board_url : $corrected_path;
        $theme          = "{$web_path}styles/" . rawurlencode($this->user->style['style_path']) . '/theme';

        return '<img class="avatar" src="' . $theme . '/images/no_avatar.gif" width="' . self::MAX_SIZE . '" height="' . self::MAX_SIZE . '" alt="">';
    }
}
