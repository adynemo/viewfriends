<?php

/**
 *
 * @package phpBB Extension - View Friends
 * @copyright (c) 2020 Ady
 * @license http://opensource.org/licenses/gpl-2.0.php GNU General Public License v2
 *
 */

namespace ady\viewfriends\controller;

use phpbb\controller\helper;
use phpbb\db\driver\driver_interface;
use phpbb\exception\http_exception;
use phpbb\language\language;
use phpbb\request\request;
use phpbb\template\template;
use phpbb\user;

class main
{

    /** @var user */
    protected $user;

    /** @var driver_interface */
    protected $db;

    /** @var request */
    protected $request;

    /** @var template */
    protected $template;

    /** @var helper */
    protected $helper;

    /** @var language */
    protected $lang;

    /** @var string root_path */
    protected $root_path;

    /** @var string php_ext */
    protected $php_ext;

    /**
     * Constructor
     *
     * @param user		        $user
     * @param driver_interface  $db
     * @param request           $request
     * @param template          $template
     * @param helper            $helper
     * @param language          $lang
     * @param string            $root_path
     * @param string            $php_ext
     */
    public function __construct(
        user              $user,
        driver_interface  $db,
        request           $request,
        template          $template,
        helper            $helper,
        language          $lang,
        $root_path,
        $php_ext
    ) {
        $this->user         = $user;
        $this->db           = $db;
        $this->request      = $request;
        $this->template     = $template;
        $this->helper       = $helper;
        $this->lang         = $lang;
        $this->root_path    = $root_path;
        $this->php_ext      = $php_ext;
    }

    public function remove_friend($friend_id)
    {
        if (!$this->user->data['is_registered'])
        {
            throw new http_exception(403, 'NOT_AUTHORISED');
        }

        $submit = $this->request->is_set_post('confirm') || $this->request->is_set_post('cancel');

        if (!$submit)
        {
            $sql = 'SELECT u.username
            FROM ' . USERS_TABLE . ' u, ' . ZEBRA_TABLE . ' z
            WHERE u.user_id = z.zebra_id
            AND ' . $this->db->sql_build_array('SELECT', ['z.zebra_id' => $friend_id]) . '
            AND ' . $this->db->sql_build_array('SELECT', ['z.user_id' => $this->user->data['user_id']]);

            $result = $this->db->sql_query($sql);
            $row    = $this->db->sql_fetchrowset($result);
            $this->db->sql_freeresult($result);

            if (empty($row))
            {
                throw new http_exception(404, 'NO_MEMBERS');
            }

            $this->lang->add_lang('common', 'ady/viewfriends');

            // Output the page
            $this->template->assign_vars([
                'VIEWFRIENDS_CONFIRM_REMOVE' => $this->lang->lang('VIEWFRIENDS_CONFIRM_REMOVE', $row[0]['username']),
            ]);

            return $this->helper->render('remove_friend.html', $this->lang->lang('VIEWFRIENDS_REMOVE_TITLE'));
        }
        else if ($this->request->is_set_post('confirm'))
        {
            $sql = 'DELETE FROM ' . ZEBRA_TABLE . ' WHERE user_id = ' . (int) $this->user->data['user_id'] . ' AND zebra_id = ' . (int) $friend_id;

            if (!$this->db->sql_query($sql)) 
            {
                trigger_error($this->lang->lang('VIEWFRIENDS_ERROR_ON_REMOVE'));
            }
        }

        redirect(append_sid("{$this->root_path}memberlist.{$this->php_ext}?mode=viewprofile&u={$this->user->data['user_id']}"));
    }
}
